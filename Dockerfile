FROM ubuntu:16.04

# 无法连接ubuntu补丁
# RUN echo "deb http://mirrors.aliyun.com/ubuntu/ xenial main"  >/etc/apt/sources.list && \
#     echo "deb-src http://mirrors.aliyun.com/ubuntu/ xenial main"  >>/etc/apt/sources.list && \
#     echo "deb http://mirrors.aliyun.com/ubuntu/ xenial-updates main"  >>/etc/apt/sources.list && \
#     echo "deb-src http://mirrors.aliyun.com/ubuntu/ xenial-updates main"  >>/etc/apt/sources.list && \
#     echo "deb http://mirrors.aliyun.com/ubuntu/ xenial universe"  >>/etc/apt/sources.list && \
#     echo "deb-src http://mirrors.aliyun.com/ubuntu/ xenial universe"  >>/etc/apt/sources.list && \
#     echo "deb http://mirrors.aliyun.com/ubuntu/ xenial-updates universe"  >>/etc/apt/sources.list && \
#     echo "deb-src http://mirrors.aliyun.com/ubuntu/ xenial-updates universe"  >>/etc/apt/sources.list && \
#     echo "deb http://mirrors.aliyun.com/ubuntu/ xenial-security main"  >>/etc/apt/sources.list && \
#     echo "deb-src http://mirrors.aliyun.com/ubuntu/ xenial-security main"  >>/etc/apt/sources.list && \
#     echo "deb http://mirrors.aliyun.com/ubuntu/ xenial-security universe"  >>/etc/apt/sources.list && \
#     echo "deb-src http://mirrors.aliyun.com/ubuntu/ xenial-security universe"  >>/etc/apt/sources.list

RUN apt-get update && apt-get install -y apache2 mariadb-server php php-curl php-gd php-ldap php-mbstring php-mcrypt php-mysql php-xml php-zip php-cli php-json curl unzip libapache2-mod-php locales

ENV LANG="en_US.UTF8"
ENV MYSQL_ROOT_PASSWORD="123456"
RUN echo -e "LANG=\"en_US.UTF-8\"\nLANGUAGE=\"en_US:en\"" > /etc/default/locale && locale-gen en_US.UTF-8

# 缓存清理
COPY marker /dev/null

RUN mkdir /app
COPY docker-entrypoint.sh /app

# 从网络下载文件
# RUN random=`date +%s`; curl http://cdn.zentaopm.com/latest/zentao.zip?rand=$random -o /var/www/zentao.zip
# COPY ./zentao.zip /var/www/zentao.zip
# RUN cd /var/www/ && unzip -q zentao.zip && rm zentao.zip

# # 将禅道文件复制到app目录下
COPY ./zentaopms /var/www/zentaopms

RUN a2enmod rewrite

RUN rm -rf /etc/apache2/sites-enabled/000-default.conf /var/lib/mysql/*
RUN sed -i '1i ServerName 127.0.0.1' /etc/apache2/apache2.conf
COPY config/apache.conf /etc/apache2/sites-enabled/000-default.conf
COPY config/ioncube_loader_lin_7.0.so /usr/lib/php/20151012/ioncube_loader_lin_7.0.so
COPY config/00-ioncube.ini /etc/php/7.0/apache2/conf.d/
COPY config/00-ioncube.ini /etc/php/7.0/cli/conf.d/

# VOLUME /app/zentaopms /var/lib/mysql
ENTRYPOINT ["sh","/app/docker-entrypoint.sh"]
