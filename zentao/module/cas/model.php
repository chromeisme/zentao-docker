<?php
/**
 * The model file of user module of ZenTaoPMS.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv12.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     user
 * @version     $Id: model.php 5005 2013-07-03 08:39:11Z chencongzhi520@gmail.com $
 * @link        http://www.zentao.net
 */
?>
<?php
class casModel extends model
{
    public function autoLoginInCas($user, $account) {
        $this->loadModel('user')->cleanLocked($user->account);
        /* Authorize him and save to session. */
        $user->rights = $this->user->authorize($account);
        $user->groups = $this->user->getGroups($account);
        $this->dao->update(TABLE_USER)->set('visits = visits + 1')->set('ip')->eq($userIP)->set('last')->eq($last)->where('account')->eq($user->account)->exec();
        
        $this->session->set('user', $user);
        $this->app->user = $this->session->user;
        $this->loadModel('action')->create('user', $user->id, 'login');
    }

    public function addUserInCas($userinfo) {
        $_POST['account'] = $userinfo['account'];
        $_POST['realname'] = $userinfo['fullname'];
        $_POST['password'] = 'NOzX8Hj2awCc';
        $_POST['password1'] = 'NOzX8Hj2awCc';
        $_POST['password2'] = 'NOzX8Hj2awCc';
        $this->loadModel('user')->autoCreate();

	if(dao::isError()) {
            // $this->send(array('result' => 'fail', 'message' => dao::getError()));
            return false;
        } else {
            //设置新增用户权限
            $userList = $this->dao->select('*')->from(TABLE_USER)->where('deleted')->eq(0)->orderBy('account')->fetchAll();
            if (count($userList) > 1 && count($userList) <= 2) {//第一个添加的非admin用户，赋予管理员权限
                $data = new stdClass();
                $data->account = $userinfo['account'];
                $data->group   = '1';
                $this->dao->insert(TABLE_USERGROUP)->data($data)->exec();
            } elseif (count($userList) > 2) {//非第一个添加的非admin用户，赋予其他权限
                $data = new stdClass();
                $data->account = $userinfo['account'];
                $data->group   = '10';
                $this->dao->insert(TABLE_USERGROUP)->data($data)->exec();
            }

            return true;
        }
    }
}
